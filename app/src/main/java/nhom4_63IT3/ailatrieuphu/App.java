package nhom4_63IT3.ailatrieuphu;

import android.app.Application;

import androidx.room.Room;

import nhom4_63IT3.ailatrieuphu.database.QuestionDataBase;

public class App extends Application {
    private static App instance;
    private Storage storage;
    private QuestionDataBase questionDB;

    public static App getInstance() {
        return instance;
    }

    public Storage getStorage() {
        return storage;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        storage = new Storage();
        initConfig();
    }

    private void initConfig() {
        questionDB = Room.databaseBuilder(this,
                QuestionDataBase.class, "Question.db").createFromAsset("Question.db").build();
    }

    public QuestionDataBase getQuestionDB() {
        return questionDB;
    }
}
