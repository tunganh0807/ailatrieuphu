package nhom4_63IT3.ailatrieuphu.database;

import nhom4_63IT3.ailatrieuphu.App;
import nhom4_63IT3.ailatrieuphu.MTask;
import nhom4_63IT3.ailatrieuphu.database.entities.HighScore;
import nhom4_63IT3.ailatrieuphu.database.entities.Question;

public class DBManager {
    private static DBManager instance;

    public DBManager() {
        // for singleton
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public void getQuestionForLevel(int level, OnResultCallBack cb) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Question question = App.getInstance().getQuestionDB().
                            getQuestionDAO().getQuestionForLevel(level);
                    cb.callBack(question);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void addScore(HighScore highScore, OnResultCallBack cb) {
        MTask mTask = new MTask(null, new MTask.MTaskListener() {
            @Override
            public Object execTask(Object dataInput, String key, MTask task) {
                try {
                    App.getInstance().getQuestionDB().getQuestionDAO().addScore((HighScore) dataInput);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            public void completeTask(Object result, String key) {
                cb.callBack(result);
            }
        });

        mTask.startAsyncTask(highScore);
    }

    public void getListHighScore(OnResultCallBack cb) {
        MTask mTask = new MTask(null, new MTask.MTaskListener() {
            @Override
            public Object execTask(Object dataInput, String key, MTask task) {
                return App.getInstance().getQuestionDB().getQuestionDAO().getListHighScore();
            }

            @Override
            public void completeTask(Object result, String key) {
                cb.callBack(result);
            }
        });

        mTask.startAsyncTask(null);
    }

    public interface OnResultCallBack {
        void callBack(Object data);
    }
}
