package nhom4_63IT3.ailatrieuphu.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import nhom4_63IT3.ailatrieuphu.database.entities.HighScore;
import nhom4_63IT3.ailatrieuphu.database.entities.Question;

import java.util.List;

@Dao
public interface QuestionDAO {

    @Query("SELECT * FROM question WHERE level = :level ORDER by random() LIMIT 1")
    Question getQuestionForLevel(int level);

    @Query("SELECT * FROM HighScore ORDER BY Score DESC LIMIT 3")
    List<HighScore> getListHighScore();

    @Insert
    void addScore(HighScore... highScore);
}
