package nhom4_63IT3.ailatrieuphu.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import nhom4_63IT3.ailatrieuphu.database.dao.QuestionDAO;
import nhom4_63IT3.ailatrieuphu.database.entities.HighScore;
import nhom4_63IT3.ailatrieuphu.database.entities.Question;

@Database(version = 1, entities = {HighScore.class, Question.class})
public abstract class QuestionDataBase extends RoomDatabase {
    public abstract QuestionDAO getQuestionDAO();
}
