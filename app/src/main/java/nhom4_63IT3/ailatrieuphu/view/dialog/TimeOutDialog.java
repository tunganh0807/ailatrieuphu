package nhom4_63IT3.ailatrieuphu.view.dialog;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import nhom4_63IT3.ailatrieuphu.R;
import nhom4_63IT3.ailatrieuphu.databinding.ViewTimeOutBinding;
import nhom4_63IT3.ailatrieuphu.view.base.BaseDialog;

public class TimeOutDialog extends BaseDialog<ViewTimeOutBinding> {
    public static final String KEY_TIME_OUT = "KEY_TIME_OUT";
    private OnTimeOutCallBack callBack;

    public TimeOutDialog(@NonNull Context context) {
        super(context);
    }

    public void setCallBack(OnTimeOutCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void initViews() {
        mBinding.btOutGame.setOnClickListener(this);
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.bt_out_game) {
            outGame();
        }
    }

    private void outGame() {
        callBack.callBack(KEY_TIME_OUT);
        dismiss();
    }

    @Override
    protected ViewTimeOutBinding initViewBinding(View view) {
        return ViewTimeOutBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_time_out;
    }

    public interface OnTimeOutCallBack {
        void callBack(String key);
    }
}
