package nhom4_63IT3.ailatrieuphu.view.activity;

import android.view.View;



import nhom4_63IT3.ailatrieuphu.MediaManager;
import nhom4_63IT3.ailatrieuphu.R;
import nhom4_63IT3.ailatrieuphu.databinding.ActivityMainBinding;
import nhom4_63IT3.ailatrieuphu.view.base.BaseActivity;
import nhom4_63IT3.ailatrieuphu.view.fragment.M000SplashFrg;
import nhom4_63IT3.ailatrieuphu.viewmodel.CommonVM;

public class MainActivity extends BaseActivity<ActivityMainBinding, CommonVM> {
    public static final String TAG = MainActivity.class.getName();

    @Override
    protected void initViews() {
        showFrg(M000SplashFrg.TAG, null, false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MediaManager.getInstance().pauseSong();
    }

    @Override
    protected void onStart() {
        super.onStart();
        MediaManager.getInstance().playSong();
    }

    @Override
    protected Class<CommonVM> initClassVM() {
        return CommonVM.class;
    }

    @Override
    protected ActivityMainBinding initViewBinding(View view) {
        return ActivityMainBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }
}