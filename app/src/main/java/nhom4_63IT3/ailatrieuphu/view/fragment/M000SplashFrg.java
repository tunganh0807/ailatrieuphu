package nhom4_63IT3.ailatrieuphu.view.fragment;

import android.os.Handler;
import android.view.View;

import nhom4_63IT3.ailatrieuphu.R;
import nhom4_63IT3.ailatrieuphu.databinding.FrgM000SplashBinding;
import nhom4_63IT3.ailatrieuphu.view.base.BaseFragment;
import nhom4_63IT3.ailatrieuphu.viewmodel.CommonVM;

public class M000SplashFrg extends BaseFragment<FrgM000SplashBinding, CommonVM> {
    public static final String TAG = M000SplashFrg.class.getName();

    @Override
    protected void initViews() {
        new Handler().postDelayed(() -> mCallBack.showFrg(M001HomeFrg.TAG, null, false), 2000);
    }

    @Override
    protected Class<CommonVM> initClassVM() {
        return CommonVM.class;
    }

    @Override
    protected FrgM000SplashBinding initViewBinding(View view) {
        return FrgM000SplashBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m000_splash;
    }
}
